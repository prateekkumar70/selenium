package test;

import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.util.Iterator;
import java.util.List;

public class NeutrogenaTestFile {
  static String url = "http://www.neutrogena.co.uk/heritage";
  WebDriver driver;
  @Test(priority=1)
  public void checkTitle() {
	  System.out.println("Checking Title");
	  String expectedTitle = "Hydro Boost� Cleansing Facial Wipes";
	  String actualTitle = driver.findElement(By.cssSelector(".block_product_detail_row1 > h1")).getText();
	  Assert.assertEquals(expectedTitle, actualTitle);
  }
  @Test(priority=2)
  public void checkRecommendations() {
	  System.out.println("Checking Recommendations");
	  String[] expectedRecommendations = {"Hydro Boost� Water Gel Moisturiser",
			  "Hydro Boost� Gel Cream Moisturiser", 
			  "Hydro Boost� Micellar Water", 
			  "Hydro Boost� Water Gel Cleanser"};
	  List<WebElement> products = driver.findElements(By.cssSelector(".pro_detail_txt"));
	  int index = 0;
	  int actual = 0;
	  for(Iterator<WebElement> itr = products.iterator(); itr.hasNext();) {
		if(itr.next().getText().equals(expectedRecommendations[index++])) {
			actual++;
		}
	  }
	  Assert.assertEquals(expectedRecommendations.length, actual);
  }
  @Test(priority=3)
  public void search() {
	  System.out.println("Searching facial wipes");
	  WebElement searchText = driver.findElement(By.cssSelector("#edit-search-key"));
	  searchText.clear();
	  searchText.sendKeys("facial wipes");
	  WebElement searchSubmit = driver.findElement(By.cssSelector("#edit-search-submit"));
	  searchSubmit.click();
  }
  @Test(priority=4)
  public void searchProductPopup() throws InterruptedException {
	  System.out.println("Checking Popup Text");
	  WebElement firstProduct = driver.findElement(By.cssSelector(".search_smoothing:nth-of-type(1)"));
	  WebElement quickViewBtn = driver.findElement(By.cssSelector(".search_smoothing:nth-of-type(1) > .search_smoothing_product > .quick_view_product_anchor"));
	  	  
	  Actions openPopup = new Actions(driver);
	  openPopup.moveToElement(firstProduct)
	  	.moveToElement(quickViewBtn)
	  	.click()
	  	.build().perform();
	  WebElement description = driver.findElement(By.cssSelector(".prod_quick_view_desc_div_p > p"));
	  openPopup 	
	  	.moveToElement(description)
	  	.build().perform();
	  System.out.println("Product Description: " + description.getText());
	  Thread.sleep(5000);
  }
  @BeforeClass
  public void beforeClass() {
	  System.setProperty("webdriver.chrome.driver", "src/resources/chromedriver.exe");
	  driver = new ChromeDriver();
	  driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  driver.manage().window().maximize();
	  driver.get(url);
	  
	  System.out.println("Hover over id=menu1");
	  Actions hoverMenuAction = new Actions(driver);
	  WebElement mainMenu = driver.findElement(By.id("menu1"));
	  hoverMenuAction.moveToElement(mainMenu);
	    
	    
	  System.out.println("Hover over id=submenu1");
	  WebElement subMenu = driver.findElement(By.id("submenu1"));
	  hoverMenuAction.moveToElement(subMenu);
	    
	  //Click on First link under .submenu1
	  WebElement firstSubMenuLink = driver.findElement(By.cssSelector(".submenu1 > ul > li:nth-of-type(4) > a"));
	  hoverMenuAction.moveToElement(firstSubMenuLink).click().build().perform();
  }

  @AfterClass
  public void afterClass() {
	  driver.quit();
  }

}
